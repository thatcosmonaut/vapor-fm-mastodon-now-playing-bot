require 'rubygems'
require 'bundler/setup'
require 'mastodon'
require 'json'
require 'net/http'

token = ''
if File.exists?('ACCESS_TOKEN')
  File.open('ACCESS_TOKEN', 'r') do |f|
    token = f.readline.strip
  end
else
  puts 'No access token. Exiting.'
  exit
end

url = 'https://vapor.fm:8000/status-json.xsl'
uri = URI(url)
response = Net::HTTP.get_response(uri)
exit if response.kind_of? Net::HTTPError
result = JSON.parse(response.body)

most_recent_song = ""
if File.exists?('most_recent_song.txt')
  File.open('most_recent_song.txt', 'r') do |f|
    most_recent_song = f.readline.strip
  end
end

song = result['icestats']['source']['title']

exit if song == most_recent_song or song.count('-') < 1

File.open('most_recent_song.txt', 'w') do |f|
  f.puts song
end

client = Mastodon::REST::Client.new(base_url: "https://plaza.vapor.fm", bearer_token: token)

toot = song
client.create_status(toot)
